package ru.kuzin.tm.api.repository;

import ru.kuzin.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}