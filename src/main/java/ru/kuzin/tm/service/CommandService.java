package ru.kuzin.tm.service;

import ru.kuzin.tm.api.repository.ICommandRepository;
import ru.kuzin.tm.api.service.ICommandService;
import ru.kuzin.tm.model.Command;

public final class CommandService implements ICommandService {

    private ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Command[] getCommands() {
        return commandRepository.getCommands();
    }

}